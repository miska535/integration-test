import express from "express";
import { after,before, describe, it } from "mocha";
import { expect } from "chai";
import routes from "../src/routes.js";

const API_URL = "http://localhost:8000/api/v1";
const PORT = 8000;
const app = express();
/** @type {import("node:http").Server} */
let server;

function newTimer() {
    return new Promise( (resolve,reject) => {
        setTimeout( () => resolve(true), 500);
    });
}
function getData() {
    return fetch(API_URL);
}

describe("REST API routes", () => {
    before(() => {
        app.use("/api/v1", routes);
        server = app.listen(PORT, () => {});

    });
    it("can get response", async () => {
        const response = await fetch(API_URL);
        expect(response.ok).to.be.true;
    });
    it("should convert RGB to hex", async () => {
        const response = await fetch(`${API_URL}/rgb-to-hex?red=255&green=136&blue=0`);
        const value = await response.text();
        expect(value).to.equal("#ff8800");
        // expect(5).to.equal(5);
    });
    it("should convert hex to RGB", async ()  => {
        // hastag character has to be endcoded
        const url = `${API_URL}/hex-to-rgb?hex=${encodeURIComponent("#ff0000")}`;
        const response = await fetch(url);
        const value = await response.text();
        expect(value).to.equal("rgb(255,0,0)");
    });
    after( function(done) {
        // closing the server may take longer than default timeout of 2000ms
        this.timeout(5000);
        server.close( () => done());
    });
});