const sum = (a,b) => {
    return a+b;
}

/**
 * Padding hex component if neccessary.
 * Hex component representation requires
 * two hexadecimal characters.
 * @param {string} comp 
 * @returns {string} two hexadecimal characters.
 */
const pad = (comp) => {
    return comp.length == 2 ? comp : "0" + comp;
}

/**
 *  Converts rgb to a hex format
 * @param {number} r RED 0-255
 * @param {number} g GREEN 0-255
 * @param {number} b BLUE 0-255
 * @returns {string} in hex color format, e.g. "#0000ff" (blue)
 */
const rgb_to_hex = (r,g,b) => {
    const HEX_RED = r.toString(16);
    const HEX_GREEN = g.toString(16);
    const HEX_BLUE = b.toString(16);
    return "#" + pad(HEX_RED) + pad(HEX_GREEN) + pad(HEX_BLUE);
};

/**
 * Converts a hex format to an array of rgb numbers
 * @param {string} hex 
 * @returns {Array.<number>}
 */
const hex_to_rgb = (hex) => {
    const split = hex.substring(1);
    const HEX_RED = split.substring(0,2);
    const HEX_GREEN = split.substring(2,4);
    const HEX_BLUE = split.substring(4);
    const RED = parseInt(HEX_RED, 16);
    const GREEN = parseInt(HEX_GREEN, 16);
    const BLUE = parseInt(HEX_BLUE, 16);
    return [RED, GREEN, BLUE];
};

export {sum, rgb_to_hex, hex_to_rgb};