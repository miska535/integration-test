import express from "express"
import routes from "./routes.js";

const PORT = 8000;
const HOST = `http://localhost:${PORT}/api/v1`;

const app = express();
app.use("/api/v1", routes);

app.listen(PORT, () => {
    console.log(`Server listening at ${HOST}`);
})
